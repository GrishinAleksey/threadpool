package com.grishin;

import com.grishin.entity.Person;
import com.grishin.threadpool.impl.FixedThreadPool;
import com.grishin.threadpool.impl.ScalableThreadPool;

public class Test {

    static FixedThreadPool fixedThreadPool = new FixedThreadPool(1);
    static ScalableThreadPool scalableThreadPool = new ScalableThreadPool(1,2);

    public static void main(String[] args) {

//        for (int i = 0; i <= 9; i++) {
//            Person person = new Person("Клиент: " + i);
//            System.out.println(person + " создан");
//            fixedThreadPool.execute(person);
//
//        }
//        fixedThreadPool.start();

        for (int i = 0; i <= 10; i++) {
            Person person = new Person("Клиент: " + i);
            System.out.println(person + " создан");
            scalableThreadPool.execute(person);
        }
        scalableThreadPool.start();
    }
}
