package com.grishin.entity;

import com.grishin.runnable.MyRunnable;

import java.util.concurrent.TimeUnit;

public class Person extends MyRunnable {

    private final String name;

    public Person(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                '}';
    }

    @Override
    public void run() {
        System.out.println(name);
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
