package com.grishin.threadpool.impl;

import com.grishin.runnable.MyRunnable;
import com.grishin.threadpool.ThreadPool;
import com.grishin.worker.Worker;

import java.util.concurrent.LinkedBlockingQueue;

public class FixedThreadPool implements ThreadPool {

    private final LinkedBlockingQueue<MyRunnable> workersQueue;
    private final Worker[] workers;

    public FixedThreadPool(int poolSize) {
        workers = new Worker[poolSize];
        workersQueue = new LinkedBlockingQueue<>();
        for (int i = 0; i < poolSize; i++) {
            workers[i] = new Worker(workersQueue);
        }
    }

    @Override
    public void start() {
        for (Worker worker : workers) {
            worker.start();
        }
    }

    @Override
    public void execute(MyRunnable runnable) {
        synchronized (workersQueue) {
            workersQueue.add(runnable);
            workersQueue.notify();
        }
    }
}
