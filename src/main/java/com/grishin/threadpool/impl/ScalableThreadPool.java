package com.grishin.threadpool.impl;

import com.grishin.runnable.MyRunnable;
import com.grishin.threadpool.ThreadPool;
import com.grishin.worker.Worker;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;

public class ScalableThreadPool implements ThreadPool {

    private final LinkedBlockingQueue<MyRunnable> workersQueue;
    private final List<Worker> workers;
    private final int minPoolSize;
    private final int maxPoolSize;
    private final Object check = new Object();


    public ScalableThreadPool(int minPoolSize, int maxPoolSize) {
        workers = new ArrayList<>();
        this.minPoolSize = minPoolSize;
        this.maxPoolSize = maxPoolSize;
        workersQueue = new LinkedBlockingQueue<>();
        for (int i = 0; i < minPoolSize; i++) {
            workers.add(new Worker(workersQueue));
        }
    }

    public int corePoolSize() {
        return workers.size();
    }

    public int getMinPoolSize() {
        return minPoolSize;
    }

    public int getMaxPoolSize() {
        return maxPoolSize;
    }

    @Override
    public void start() {
        Thread thread = new Thread(this::optimizeSize);
        thread.setDaemon(true);
        thread.start();
        for (Worker worker : workers) {
            worker.start();
        }
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void execute(MyRunnable runnable) {
        synchronized (workersQueue) {
            workersQueue.add(runnable);
            workersQueue.notify();
        }
    }

    public void optimizeSize() {
        while (true) {
            synchronized (check) {
                if (workersQueue.size() > 0) {
                    if (corePoolSize() < getMaxPoolSize()) {
                        System.out.println("Новый поток");
                        Worker worker = new Worker(workersQueue);
                        workers.add(worker);
                        worker.start();
                    }
                } else {
                    for (int i = minPoolSize; i < workers.size(); i++) {
                            System.out.println("удаление потока");
                            if (corePoolSize() > getMinPoolSize()) {
                                workers.get(i).interrupt();
                                workers.remove(i--);
                        }
                    }
                }
            }
        }
    }
}
