package com.grishin.worker;

import com.grishin.runnable.MyRunnable;

import java.util.concurrent.LinkedBlockingQueue;

public class Worker extends Thread {

    private final LinkedBlockingQueue<MyRunnable> queue;

    public Worker(LinkedBlockingQueue<MyRunnable> queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        Runnable task;
        while (true) {
            synchronized (queue) {
                while (queue.isEmpty()) {
                    try {
                        queue.wait();
                    } catch (InterruptedException e) {
                        System.out.println("Ошибка ожидания очереди: " + e.getMessage());
                    }
                }
                task = queue.poll();
            }
            try {
                task.run();
            } catch (RuntimeException e) {
                System.out.println("Пул потоков прерван");
            }
        }
    }
}
